Usage: 

```
[mono] narupa-net-test.exe
```

Options:

```
  -u, --uri=VALUE            server host URI (e.g. ws://localhost:8080)
  -o, --output=VALUE         output CSV path for results (default results.csv)
  -s, --samples=VALUE        how many frames to gather data (default 500)
  -p, --simulationPaths=VALUE
                             file containing simulation paths on server to run (default simulationPaths.txt)
  -h, --help                 show this message and exit
```
