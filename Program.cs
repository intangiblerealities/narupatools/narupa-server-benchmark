﻿// Copyright (c) Mike O'Connor, University of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Mono.Options;
using narupa_net_test;
using Nano;
using Nano.Client;
using Nano.Client.Console.Simbox;
using Nano.Transport.Variables;

namespace Narupa.NetTest
{
    /// <summary>
    /// A little script that connects to a server, runs a bunch of simulations, and outputs performance data.
    /// </summary>
    internal class Program
    {
        private static NSBFrameReader reader;
        private ManualResetEvent connectedEvent = new ManualResetEvent(false);
        private static int framesReceived;
        private IReporter reporter;

        private Stopwatch clock = new Stopwatch();
        private Queue<BenchmarkData> sampleQueue = new Queue<BenchmarkData>();
        private Uri uri;
        ManualResetEvent reconnectionEvent = new ManualResetEvent(false);
        private string outputPath;

        public static void Main(string[] args)
        {
            var shouldShowHelp = false;
            string uri = "ws://130.61.53.21:8080";
            int samples = 300;
            string simulationPathFile = "simulationPaths.txt";
            string output = "results.csv";
            
            var options = new OptionSet { 
                { "u|uri=", "server host URI", h => uri = h }, 
                { "o|output=", "output CSV path for results", o => output = o},
                { "s|samples=", "how many frames to gather data (default 500)", (int t) => samples = t},
                { "p|simulationPaths=", "file containing simulation paths on server to run", (s) => simulationPathFile = s},
                { "h|help", "show this message and exit", h => shouldShowHelp = h != null },
            };

           
            var extras = options.Parse(args);

            if (shouldShowHelp)
            {
                options.WriteOptionDescriptions(Console.Out);
                return;
            }
            var program = new Program();
            try
            {
                program.RunBenchark(uri, samples, simulationPathFile, output);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception thrown: {e.Message} ");
            }
            
        }
        
        private static string GetOpenClientKey()
        {
            byte[] clientKeyBytes = new byte[]
            {
                227, 189, 198, 86,
                117, 173, 114, 171,
                7,   52,  3,   198,
                37,  220, 61,  79,
                135, 170, 76,  100,
                93,  248, 182, 55,
                196, 170, 86,  165,
                246, 127, 40,  33
            };

            return Convert.ToBase64String(clientKeyBytes);
        }

        private IEnumerable<string> LoadSimulationPaths(string path)
        {
            return File.ReadLines(path);
        }
        
        //Runs the benchmark
        private void RunBenchark(string uriString, int samples, string simulationPathFile, string output)
        {
            outputPath = output;
            reporter = CmdReporter.Instance;
            reporter.ReportVerbosity = ReportVerbosity.Normal;
            reporter.OscMessageFilter = new DefaultOscMessageFilter {FilterMode = DefaultOscMessageFilterMode.None}; 

            uri = null;
            try
            {
                uri = new Uri(uriString);
            }
            catch (Exception e)
            {
                reporter.PrintError($"Error: Failed to parse URI {uriString}");
            }

            
            //Sets up the cloud connection.
            reader = new NSBFrameReader();
            var key = GetOpenClientKey();
            CloudConnectionInfo connectionInfo = new CloudConnectionInfo("Narupa Server", uri, key);

            ConnectToServer(connectionInfo);

            //Wait for connection to succeed
            bool success = connectedEvent.WaitOne(5000);
            if (!success)
            {
                reporter.PrintError("Connection timed out.");
                return;
            }
            
            reporter.PrintEmphasized("Connected!");

            try
            {
                //Run each simulation, collecting data.
                foreach (var simulationPath in LoadSimulationPaths(simulationPathFile))
                {
                    RunSimulation(simulationPath, samples);
                }

            }
            catch (Exception e)
            {
                reporter.PrintException(e, "Exception thrown running simulation.");
            }
            finally
            {
                reader.Client.Dispose();
            }
        }

        private bool SendLoadSimulation(string simulationPath)
        {
            if (reader.Ready == false)
            {
                reporter.PrintError("Cannot load simulation, not connected!");
                return false;
            }
            reader.Client.SendSimulationPath(simulationPath);
            reconnectionEvent.Reset();
            return true;
        }
        
        private void RunSimulation(string simulationPath, int samples)
        {
            //Load simulation
            reconnectionEvent.Reset();
            SendLoadSimulation(simulationPath);
            reporter.PrintEmphasized($"Waiting for simulation {simulationPath} to load, this can take a while...");
            // give the simulation a full 5 minutes to load.
            bool success = reconnectionEvent.WaitOne(3000000);
            if (!success)
                throw new Exception($"Timeout waiting for simulation {simulationPath} to load.");
            
            //Hang about until all the frames have been received.
            reader.ReceivedFrame += OnReceivedFrame;
            while (framesReceived < samples)
            {
                Thread.Sleep(1000);
                reporter.PrintNormal($"Sampling simulation {simulationPath}... {framesReceived} frames received");
            }
            reader.ReceivedFrame -= OnReceivedFrame;

            //Append the data to the csv file.
            reporter.PrintEmphasized($"Finished sampling, outputting data to {outputPath} ");
            lock (sampleQueue)
            {
                WriteFrames(outputPath, simulationPath, uri.ToString(), reader.LatestFrame.AtomCount, sampleQueue);
            }
            
            ResetBenchmark();
        }

        private void ResetBenchmark()
        {
            clock.Stop();
            clock.Reset();
            lock (sampleQueue)
            {
                sampleQueue.Clear();
            }

            framesReceived = 0;
        }
        
        private async Task ConnectToServer(CloudConnectionInfo info)
        {
            reader.OnReady += OnConnectedAndReady;
            reader.TryEnterServer(info, null,null,null, info.SessionKey);
        }

        private void OnConnectedAndReady()
        {
            connectedEvent.Set();
            reader.Client.SubscribeMessage("/reconnect", OnReconnectReceived);

        }

        private void OnReconnectReceived(string arg1, object[] arg2)
        {
            reconnectionEvent.Set();
        }

        private void WriteFrames(string path, string simulationPath, string host, int numberOfAtoms, IEnumerable<BenchmarkData> samples)
        {
            string newFileName = path;

            if (!File.Exists(newFileName))
            {
                string header = "Simulation Path, Host, Number of Particles, Receive Time, Compute Time"  + Environment.NewLine;

                File.WriteAllText(newFileName, header);
            }
            
            StringBuilder builder = new StringBuilder();
            foreach (var sample in samples)
            {
                builder.AppendLine($"{simulationPath},{host},{numberOfAtoms},{sample.FrameReceiveTime},{sample.FrameComputeTime}");
            }
            
            File.AppendAllText(newFileName, builder.ToString());
        }
        
        
        private void OnReceivedFrame()
        {
            
            if (clock.IsRunning == false)
            {
                clock.Start();
            }
            else
            {
                clock.Stop();
                var time = clock.ElapsedMilliseconds;
                var computeTimeVar = reader.Client.VariablePortal.Variables[VariableName.ComputationTime];
                var computeTime = ((IVariable<float>) computeTimeVar).Value;
                var data = new BenchmarkData(time, computeTime);
                lock (sampleQueue)
                {
                    sampleQueue.Enqueue(data);
                }
                clock.Reset();
                clock.Start();
            }
            framesReceived++; 
            
        }
    }
}