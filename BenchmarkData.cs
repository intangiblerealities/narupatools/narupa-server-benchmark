﻿// Copyright (c) Mike O'Connor, University of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace narupa_net_test
{
    internal class BenchmarkData
    {
        public readonly long FrameReceiveTime;
        public readonly float FrameComputeTime;

        public BenchmarkData(long frameReceiveTime, float frameComputeTime)
        {
            FrameReceiveTime = frameReceiveTime;
            FrameComputeTime = frameComputeTime;
        }
        
    }
}